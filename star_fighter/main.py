import sys
import pygame
from random import randint

pygame.init()

game_font = pygame.font.Font(None, 30)
screen_width, screen_height = 800, 600
screen_fill_color = (32, 52, 71)
fighter_speed = 0.5
missile_speed = 0.3
alien_speed = 0.1
fighter_is_moving_left, fighter_is_moving_right = False, False
missile_was_fired = False
game_is_running = True

screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Star fighter")

# importing images
fighter_image = pygame.image.load('images/fighter.png')
missile_image = pygame.image.load('images/missile.png')
alien_image = pygame.image.load('images/alien.png')

# getting size of .png: fighter, missile, alien
fighter_width, fighter_height = fighter_image.get_size()
missile_width, missile_height = missile_image.get_size()
alien_width, alien_height = alien_image.get_size()

# setting original spawn of fighter, missile, alien
fighter_x, fighter_y = screen_width / 2 - fighter_width / 2, screen_height - fighter_height
missile_x, missile_y = fighter_x + fighter_width / 2 - missile_width / 2, fighter_y - missile_height
alien_x, alien_y = randint(0, screen_width - alien_width), 0

# create masks for images
fighter_mask = pygame.mask.from_surface(fighter_image)
missile_mask = pygame.mask.from_surface(missile_image)
alien_mask = pygame.mask.from_surface(alien_image)


def collide_mask(x1, y1, mask1, x2, y2, mask2):
    offset = (int(x2 - x1), int(y2 - y1))
    overlap = mask1.overlap(mask2, offset)
    return overlap is not None


game_score = 0

while game_is_running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        # if getting sme input
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                fighter_is_moving_left = True
            if event.key == pygame.K_RIGHT:
                fighter_is_moving_right = True
            if event.key == pygame.K_SPACE:
                missile_was_fired = True
                missile_x, missile_y = fighter_x + fighter_width / 2 - missile_width / 2, fighter_y - missile_height

        # if button was released
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                fighter_is_moving_left = False
            if event.key == pygame.K_RIGHT:
                fighter_is_moving_right = False

    # movement of fighter
    if fighter_is_moving_left and fighter_x >= fighter_speed:
        fighter_x -= fighter_speed
    if fighter_is_moving_right and fighter_x <= screen_width - fighter_width - fighter_speed:
        fighter_x += fighter_speed

    # movement of alien
    alien_y += alien_speed

    # if missile is out of screen, missile is disappearing
    if missile_was_fired and missile_y + missile_height < 0:
        missile_was_fired = False

    # moving missile up towards alien
    if missile_was_fired:
        missile_y -= missile_speed

    screen.fill(screen_fill_color)
    # showing fighter on the screen based on the coordinates
    screen.blit(fighter_image, (fighter_x, fighter_y))

    # showing alien on the screen
    screen.blit(alien_image, (alien_x, alien_y))

    # showing missile on the screen if missile was fired
    if missile_was_fired:
        screen.blit(missile_image, (missile_x, missile_y))

    # reflecting game score
    game_score_text = game_font.render(f"Score {game_score}", True, 'white')
    screen.blit(game_score_text, (20, 20))

    pygame.display.update()

    # condition when alien hits bottom of the screen
    if alien_y + alien_height > screen_height:
        game_is_running = False

    # check collision between missile and alien using masks + increasing alien/fighter speed
    if missile_was_fired and collide_mask(missile_x, missile_y, missile_mask, alien_x, alien_y, alien_mask):
        missile_was_fired = False
        alien_x, alien_y = randint(0, screen_width - alien_width), 0
        alien_speed += 0.01
        fighter_speed += 0.06
        missile_speed += 0.06
        game_score += 1


# ending the game and showing the text with 5 sec delay
game_over_text = game_font.render("Game Over", True, 'red')
game_over_rectangle = game_over_text.get_rect()
game_over_rectangle.center = (screen_width / 2, screen_height / 2)
screen.blit(game_over_text, game_over_rectangle)
pygame.display.update()
pygame.time.wait(5000)
pygame.quit()
