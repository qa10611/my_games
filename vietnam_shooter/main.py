import pygame

pygame.init()


SCREEN_WIDTH = 800
SCREEN_HEIGHT = int(SCREEN_WIDTH * 0.8)

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('Vietnam Shooter')
game_is_running = True

# set frame rate
clock = pygame.time.Clock()
FPS = 60

# define player action variables
moving_left = False
moving_right = False

# define colors
BACKGROUND = (32, 35, 71)


def draw_background():
    screen.fill(BACKGROUND)


# inheriting from Sprite gaining access to functionality that allows easily manage and manipulate sprites within game.
# This includes features like collision detection, grouping, and rendering.
class Soldier(pygame.sprite.Sprite):
    def __init__(self, char_type, x, y, scale, speed):
        pygame.sprite.Sprite.__init__(self)
        self.char_type = char_type
        self.speed = speed
        self.direction = 1
        self.flip = False

        img = pygame.image.load(f'img/{self.char_type}/Idle/0.png')
        # getting actual width and height and scaling the image
        self.image = pygame.transform.scale(img, (int(img.get_width() * scale), int(img.get_height() * scale)))
        self.rect = self.image.get_rect()  # getting size of imported image
        self.rect.center = (x, y)

    def move(self, moving_left, moving_right):
        # reset movement variables
        change_x = 0
        change_y = 0

        # assign movement variables if moving left or right
        if moving_left:
            change_x = -self.speed
            self.flip = True
            self.direction = -1
        if moving_right:
            change_x = +self.speed
            self.flip = False
            self.direction = 1

        # update player's rectangle position
        self.rect.x += change_x
        self.rect.y += change_y

    # drawing player on the screen
    def draw(self):
        # flipping the image if player walks to opposite side (to the left)
        screen.blit(pygame.transform.flip(self.image, self.flip, False), self.rect)


player = Soldier('player', 200, 200, 3, 5)


while game_is_running:

    clock.tick(FPS)

    draw_background()
    player.draw()

    player.move(moving_left, moving_right)

    for event in pygame.event.get():
        # if user close the game window -> exit the game
        if event.type == pygame.QUIT:
            game_is_running = False

        # keyboard button pressed
        if event.type == pygame.KEYDOWN:
            # player movement left and right using A D buttons
            if event.key == pygame.K_a:
                moving_left = True
            if event.key == pygame.K_d:
                moving_right = True
            # if esc pressed -> quit the game
            if event.key == pygame.K_ESCAPE:
                game_is_running = False

        # keyboard button released
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_a:
                moving_left = False
            if event.key == pygame.K_d:
                moving_right = False

    pygame.display.update()

pygame.quit()
